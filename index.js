'use strict';

var hls = new Hls();
var video = document.getElementById('video');
var defaultVideo = 'https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8';

var setVideo = function (url) {
    hls.loadSource(url);
    hls.attachMedia(video);
    hls.on(Hls.Events.MANIFEST_PARSED, function() {
        video.play();
    });
};

setVideo(defaultVideo);

var updateInfo = function () {
    var level = hls.levels[hls.currentLevel];

    $('#bitrateDisplay').text(level.bitrate + ' bps');
    $('#heightDisplay').text(level.height + ' px');
    $('#widthDisplay').text(level.width + ' px');
    $('#videoCodecDisplay').text(level.videoCodec);
    $('#audioCodecDisplay').text(level.audioCodec);
};

var setQualityValues = function () {
    var levels = hls.levels;
    var kbps, text, btnSelector;

    for(var i = 0; i < levels.length; i++){
        btnSelector ='<button class="btn btn-info" type="button" ' +
            'onclick="changeQuality(' + i + ')"></button>';

        kbps = (levels[i].bitrate / 1000).toFixed(0);
        text = kbps + ' kbps';

        $('#qualityGroup').append( $(btnSelector).text(text));
    }
};

var setSubtitleValues = function (subtitles) {
    var optionSelector;
    for(var i = 0; i < subtitles.length; i++){
        optionSelector = '<option value="'+ i + '"></option>';
        $('#subtitles').append( $(optionSelector).text('Language: ' + subtitles[i].name));
    }
};

hls.on(Hls.Events.MANIFEST_LOADED, function (event, data) {
    cleanButtons();
    setQualityValues();

    if(data.subtitles.length > 0 ){
        $('#defaultSubs').remove();
        setSubtitleValues(data.subtitles);
    } else {
        $('#subtitles').append('<option id="defaultSubs">No available subtitles</option>');
    }
});

hls.on(Hls.Events.LEVEL_SWITCHED, function () {
    updateInfo();
});

hls.on(Hls.Events.MANIFEST_LOADING, function (event, data) {
    logRequest('Manifest loading: \t' + data.url + ' \n');
});

hls.on(Hls.Events.LEVEL_LOADING, function (event, data) {
    logRequest('Level loading: \t\t' + data.url + ' \n');
});

hls.on(Hls.Events.FRAG_LOADING, function (event, data) {
    logRequest('Fragment loading: \t' + data.frag.url + ' \n');
});

hls.on(Hls.Events.SUBTITLE_TRACK_LOADING, function (event, data) {
    logRequest('Subtitle loading: \t' + data.url + ' \n');
});

hls.on(Hls.Events.AUDIO_TRACK_LOADING, function (event, data) {
    logRequest('Audio track loading: \t' + data.url + ' \n');
});

var changeQuality = function (num) {
    hls.currentLevel = num;
};

$('#urlBtn').click(function () {
    var urlVal = $('#url').val();
    if (urlVal && urlVal.endsWith('m3u8')) {
        setVideo(urlVal);
        $('#url').val('');
    }
});

$('#videoSize').change(function () {
    var sizeVal = $('#videoSize').val();
    $('video').width(sizeVal);
});

$('#subtitles').change(function () {
    hls.subtitleTrack = $('#subtitles').val();
});

$('#exampleVideo').change(function () {
   setVideo($('#exampleVideo').val());
});

var cleanButtons = function () {
    $('#subtitles').empty();
    $('#qualityGroup').empty();
};

var logRequest = function (msg) {
    $('#urlLog').append(msg);
};